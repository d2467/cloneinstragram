from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from posts import views


urlpatterns = [
    path('', views.list_posts,  name='feed'),
]
